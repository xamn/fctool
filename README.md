# FCTool
A fun scripting language for simple shell scripts. Just for fun!

### Installation
1. [Install Cargo](https://doc.rust-lang.org/cargo/getting-started/installation.html)
2. Run `cargo install fctool`

### Usage
`fctool {input file}`

### Examples

See the 'examples' directory in the repository.

### Doc
Hello world
```
Hello, world!
```

Write to and overwrite a file
```
![file.txt]
```

Write but don't overwrite
```
[file.txt]
```

Define a variable
```
var = text
```

Reference a variable
```
{var}
```

Read from pipe
```
%foriter
    line {line_i}: {line}
%endfor
```

Iterate lines of specified expression
```
%for line : {$["text.txt"]}
    prefix {line} suffix
%endfor
```

Iterate with specified split string of specified expression
```
%for line : {$["/etc/passwd"]}
    %for column : {colon} : {line}
        {column}
    %endfor
    %l
%endfor
```

Execute a command
```
{$("echo 1")}
```

Read a file
```
{$["file.txt"]}
```

If else
```
%if {var == "value" || other == "foobar"}
    condition was true!
%elif {var == "foo"}
    Elif true!
%else
    Else
%endif
```

User input
```
user_input = {input("enter value: ")}
```

Exit process
```
{exit}
```

Source another fctool-file
```
{source("other.fctool")}
```

Math!
```
{x + y} Add
{x - y} Sub
{x * y} Mul
{x / y} Div
{x % y} Mod
{x ^ y} Pow
```

Functions
```
%func add a b
    %ret {a + b}
%endfunc

{add(1, 2)}
```

Regex matching
```
%if {match(".+", "abc")}
    matches!
%endif
```

Line break
```
%l
```

Input text into a command
```
$$[cat]
hello
to
cat
```

Use control character variables  
{   left_brace  
}   right_brace  
:   colon  
;   semicolon  
%   percent  