#!/usr/bin/env bash

# -l option wraps code inside a foriter loop

cat /etc/passwd | fctool -li "usename: {nth(line, colon, 0)};shell:   {nth(line, colon, 6)};%l"