
mod opt;

use lalrpop_util::lalrpop_mod;
lalrpop_mod!(pub grammar);

use regex::Regex;
use lazy_static::lazy_static;
use structopt::StructOpt;
use std::collections::HashMap;
use std::io::{Write, Read};
use lalrpop_util::ParseError;

lazy_static! {
    static ref INJECT_REGEX: Regex = Regex::new("\\{[^}]+}").unwrap();
    static ref VAR_REGEX: Regex = Regex::new("^([a-zA-Z_]+)\\s*=\\s*(.+)").unwrap();
    static ref COMMENT_REGEX: Regex = Regex::new(r#"(?m)("(\\.|[^"])*")|(#.*$)"#).unwrap();
    static ref PARSER: grammar::ExprParser = grammar::ExprParser::new();
}

use std::process::Command;

enum File {
    Regular(std::fs::File, String),
    Stdout,
    Stderr,
    Null,
    Process(std::process::Child),
    Variable(String),
}

enum IfState {
    Bool(bool),
    BranchAlreadyExecuted,
}

struct State {
    file: Vec<File>,
    variables: Vec<HashMap<String, String>>,
    functions: HashMap<String, FuncData>,
    if_stack: Vec<IfState>,
    for_data: Option<ForData>,
    func_data: Option<FuncData>,
    loop_state: LoopState,
}

enum LoopState {
    None,
    Continue,
    Break,
    Return(String),
}

#[derive(Debug)]
struct ForData {
    var_name: String,
    input: Option<String>,
    lines: Vec<String>,
    stack: usize,
    start_line_nr: usize,
    sep: Option<String>,
}

struct FuncData {
    name: String,
    arg_names: Vec<String>,
    lines: Vec<String>,
    filename: String,
    start_line_nr: usize,
}

fn main() {
    let opt = opt::Opt::from_args();
    let mut state = State {
        file: vec![File::Stdout],
        variables: vec![HashMap::new()],
        functions: HashMap::new(),
        if_stack: vec![IfState::Bool(true)],
        for_data: None,
        func_data: None,
        loop_state: LoopState::None,
    };

    state.variables[0].insert("true".to_owned(), "true".to_owned());
    state.variables[0].insert("false".to_owned(), "false".to_owned());
    state.variables[0].insert("left_brace".to_owned(), "{".to_owned());
    state.variables[0].insert("right_brace".to_owned(), "}".to_owned());
    state.variables[0].insert("colon".to_owned(), ":".to_owned());
    state.variables[0].insert("semicolon".to_owned(), ";".to_owned());
    state.variables[0].insert("percent".to_owned(), "%".to_owned());


    if opt.linemode {
        parse_line(&mut state, "prefix", 0, "%foriter");
    }

    for var in opt.vars {
        let split: Vec<&str> = var.split('=').collect();
        if split.len() == 1 {
            state.variables[0].insert(split[0].to_owned(), "true".to_owned());
            continue;
        }
        if split.len() != 2 {
            eprintln!("fctool: invalid argument {}", var);
            continue;
        }
        state.variables[0].insert(split[0].to_owned(), split[1].to_owned());
    }

    process_file(&mut state, "prelude", include_str!("prelude.fctool"));

    match opt.input {
        Some(input) => {
            match &opt.filename {
                Some(filename) => {
                    match std::fs::read_to_string(&filename) {
                        Ok(text) => {
                            process_file(&mut state, &filename, &text);
                        }
                        Err(err) => {
                            eprintln!("fctool: {}: {}", filename, err);
                        }
                    }
                }
                None => {}
            }
            for statement in input.split(";")  {
                let mut line = statement.to_owned();
                line.push('\n');
                parse_line(&mut state, "<stdin>", 0, &line);
            }
            if opt.linemode {
                parse_line(&mut state, "prefix", 0, "%endfor");
            }
            return;
        }
        None => {}
    }

    match opt.filename {
        Some(filename) => {
            match std::fs::read_to_string(&filename) {
                Ok(text) => {
                    process_file(&mut state, &filename, &text);
                }
                Err(err) => {
                    eprintln!("fctool: {}: {}", filename, err);
                }
            }
        }
        None => {
            let mut line_nr = 0;
            loop {
                let mut line = String::new();
                match std::io::stdin().read_line(&mut line) {
                    Ok(0) => break,
                    Ok(_) => {}
                    Err(_) => break,
                }
                parse_line(&mut state, "<stdin>", line_nr,  &line);
                line_nr += 1;
            }
        }
    }

    if opt.linemode {
        parse_line(&mut state, "prefix", 0, "%endfor");
    }
}

fn process_file(state: &mut State, filename: &str, contents: &str) {
    for (line_nr, line) in contents.lines().enumerate() {
        let mut line = line.to_owned();
        line.push('\n');
        parse_line(state, filename, line_nr, &line);
    }
}

pub struct Loc {
    pub filename: String,
    pub line: usize,
    pub index: usize,
}

pub struct Expr {
    data: ExprData,
    loc: Loc,
}

pub enum ExprData {
    String(String),
    Command(Box<Expr>),
    File(Box<Expr>),
    Concat(Box<Expr>, Box<Expr>),
    Variable(String),
    Eq(Box<Expr>, Box<Expr>),
    Neq(Box<Expr>, Box<Expr>),
    And(Box<Expr>, Box<Expr>),
    Or(Box<Expr>, Box<Expr>),
    Input(Option<Box<Expr>>),
    Exit,
    Source(Box<Expr>),
    Add(Box<Expr>, Box<Expr>),
    Sub(Box<Expr>, Box<Expr>),
    Mul(Box<Expr>, Box<Expr>),
    Div(Box<Expr>, Box<Expr>),
    Mod(Box<Expr>, Box<Expr>),
    Pow(Box<Expr>, Box<Expr>),
    LessThan(Box<Expr>, Box<Expr>),
    MoreThan(Box<Expr>, Box<Expr>),
    LessThanOrEq(Box<Expr>, Box<Expr>),
    MoreThanOrEq(Box<Expr>, Box<Expr>),
    Nth(Box<Expr>, Option<Box<Expr>>, Box<Expr>),
    Stdin,
    Eval(Box<Expr>),
    Call(String, Vec<Expr>),
    Match(Box<Expr>, Box<Expr>),
    In(Box<Expr>, Box<Expr>),
    Trim(Box<Expr>, Option<Box<Expr>>),
    Filter(Box<Expr>, Option<Box<Expr>>, Box<Expr>),
    Exclude(Box<Expr>, Option<Box<Expr>>, Box<Expr>),
}


fn parse_line(state: &mut State, filename: &str, line_nr: usize, command_line: &str) {

    let command_line = COMMENT_REGEX.replace_all(command_line, "${1}").into_owned();

    let mut command_line = command_line.trim_start();

    if command_line.starts_with("#") || command_line == "" {
        return;
    }

    if command_line.ends_with("\\\n") || command_line.ends_with("\\\r\n") {
        command_line = command_line.trim_end();
        command_line = &command_line[0..command_line.len()-1]
    }

    if let Some(func_data) = &mut state.func_data {
        if command_line.starts_with("%endfunc") {
            let func_data = state.func_data.take().unwrap();
            state.functions.insert(func_data.name.clone(), func_data);
        } else {
            func_data.lines.push(command_line.to_owned());
        }
        return;
    }

    let mut for_ended = false;

    if let Some(for_data) = &mut state.for_data {
        if command_line.starts_with("%endfor") {
            if for_data.stack == 0 {
                for_ended = true;
            } else {
                for_data.lines.push(command_line.to_owned());
                for_data.stack -= 1;
                return;
            }
        } else if command_line.starts_with("%for ") && command_line.trim_end() != "%for" {
            for_data.lines.push(command_line.to_owned());
            for_data.stack += 1;
            return;
        } else {
            for_data.lines.push(command_line.to_owned());
            return;
        }
    }

    if for_ended {
        let for_data = state.for_data.take().unwrap();
        match for_data.sep {
            Some(sep) => {
                let input = for_data.input.unwrap();
                let mut split: Vec<&str> = input.split(&sep).collect();
                if split.last().unwrap() == &"" {
                    split.pop();
                }
                'outer: for (input_line_i, input_line) in split.into_iter().enumerate() {
                    state.variables.last_mut().unwrap().insert(for_data.var_name.to_owned(), input_line.to_owned());
                    state.variables.last_mut().unwrap().insert(format!("{}_i", for_data.var_name), input_line_i.to_string());
                    let mut line_nr = for_data.start_line_nr;
                    for code_line in &for_data.lines {
                        parse_line(state, filename, line_nr, code_line);
                        match state.loop_state {
                            LoopState::None => {}
                            LoopState::Continue => {
                                state.loop_state = LoopState::None;
                                continue 'outer;
                            }
                            LoopState::Break => {
                                state.loop_state = LoopState::None;
                                break 'outer;
                            }
                            LoopState::Return(_) => {
                                break 'outer;
                            }
                        }
                        line_nr += 1;
                    }
                }
            }
            None => {
                let stdin = std::io::stdin();
                let mut input_line_nr = 0;
                for input_line in stdin.lines() {
                    state.variables.last_mut().unwrap().insert("line_i".to_owned(), input_line_nr.to_string());
                    if let Ok(input_line) = input_line {
                        state.variables.last_mut().unwrap().insert(for_data.var_name.to_owned(), input_line.to_owned());
                        let mut line_nr = for_data.start_line_nr;
                        for code_line in &for_data.lines {
                            parse_line(state, filename, line_nr, code_line);
                            line_nr += 1;
                        }
                    }
                    input_line_nr += 1;
                }
            }
        }
        return;
    }

    if command_line.starts_with("%if ") && command_line.trim_end() != "%if" {
        let mut command_line = command_line.trim_end()["%if ".len()..].to_owned();
        process_line(state, filename, line_nr, &mut command_line);
        state.if_stack.push(IfState::Bool(&command_line == "true"));
        return;
    } else if command_line.starts_with("%endif") {
        let _ = state.if_stack.pop();
        return;
    } else if command_line.starts_with("%else") {
        match state.if_stack.last_mut() {
            Some(IfState::Bool(a@false)) => *a = !*a,
            _ => {}
        }
        return;
    } else if command_line.starts_with("%elif ") && command_line.trim_end() != "%elif" {
        let mut command_line = command_line.trim_end()["%elif ".len()..].to_owned();
        process_line(state, filename, line_nr, &mut command_line);
        match state.if_stack.last_mut() {
            Some(a@IfState::Bool(true)) => {
                *a = IfState::BranchAlreadyExecuted;
            }
            Some(a@IfState::Bool(false)) => {
                *a = IfState::Bool(&command_line == "true")
            }
            _ => {}
        }
        return;
    }

    for if_state in &state.if_stack {
        if let IfState::Bool(true) = if_state { }
        else {
            return;
        }
    }

    if command_line.starts_with("%cnt") {
        state.loop_state = LoopState::Continue;
        return;
    }
    if command_line.starts_with("%brk") {
        state.loop_state = LoopState::Break;
        return;
    }
    if command_line.starts_with("%ret ") && command_line != "%ret " {
        let mut command_line = command_line.trim_end()["%ret ".len()..].to_owned();
        process_line(state, filename, line_nr, &mut command_line);
        state.loop_state = LoopState::Return(command_line);
        return;
    }

    if command_line.trim_end() == "[/]" {
        let _ = state.file.pop();
    } else if command_line.starts_with('[') && command_line.trim_end().ends_with(']') {
        let command_line = command_line.trim_end();
        let mut command_line = command_line[1..command_line.len() - 1].trim().to_owned();
        process_line(state, filename, line_nr, &mut command_line);
        if command_line == "stdout" {
            state.file.push(File::Stdout);
        } else if command_line == "stderr" {
            state.file.push(File::Stderr);
        } else if command_line == "null" {
            state.file.push(File::Null);
        } else {
            match std::fs::OpenOptions::new().write(true).create(true).append(true).open(&command_line) {
                Ok(file) => {
                    state.file.push(File::Regular(file, command_line.to_owned()));
                }
                Err(err) => {
                    eprintln!("fctool: {}: {}: {}: {}", filename, line_nr + 1, command_line, err);
                }
            }
        }
    } else if command_line.starts_with("![") && command_line.trim_end().ends_with(']') {
        let command_line = command_line.trim_end();
        let mut command_line = command_line[2..command_line.len() - 1].trim().to_owned();
        process_line(state, filename, line_nr, &mut command_line);
        if command_line == "stdout" {
            state.file.push(File::Stdout);
        } else if command_line == "stderr" {
            state.file.push(File::Stderr);
        } else if command_line == "null" {
            state.file.push(File::Null);
        } else {
            match std::fs::OpenOptions::new().write(true).create(true).append(false).truncate(true).open(&command_line) {
                Ok(file) => {
                    state.file.push(File::Regular(file, command_line.to_owned()));
                }
                Err(err) => {
                    eprintln!("fctool: {}: {}: {}: {}", filename, line_nr + 1, command_line, err);
                }
            }
        }
    } else if command_line.starts_with("%[") && command_line.trim_end().ends_with(']') {
        let command_line = command_line.trim_end();
        let command_line = command_line[2..command_line.len() - 1].trim().to_owned();
        state.file.push(File::Variable(command_line));
    } else if command_line.starts_with("$$[") && command_line.trim_end().ends_with(']') {
        let command_line = command_line.trim_end();
        let command_line = command_line[3..command_line.len() - 1].trim().to_owned();
        match Command::new("sh")
        .arg("-c")
        .arg(command_line)
        .stdin(std::process::Stdio::piped())
        .spawn() {
            Ok(proc) => state.file.push(File::Process(proc)),
            Err(_) => {}
        }
        
    } else if command_line.starts_with("%for ") && command_line.trim_end() != "%for" {
        let command_line = command_line.trim_end()["%for ".len()..].to_owned();
        let mut splitted: Vec<String> = command_line.split(':').map(|a| a.to_owned()).collect();
        if splitted.len() == 2 {
            process_line(state, filename, line_nr, &mut splitted[1]);
            state.for_data = Some(ForData {
                var_name: splitted[0].trim().to_owned(),
                input: Some(splitted[1].trim_start().to_owned()),
                lines: vec![],
                stack: 0,
                start_line_nr: line_nr,
                sep: Some("\n".to_owned()),
            });
        } else if splitted.len() == 3 {
            process_line(state, filename, line_nr, &mut splitted[1]);
            process_line(state, filename, line_nr, &mut splitted[2]);
            state.for_data = Some(ForData {
                var_name: splitted[0].trim().to_owned(),
                input: Some(splitted[2].trim_start().to_owned()),
                lines: vec![],
                stack: 0,
                start_line_nr: line_nr,
                sep: Some(splitted[1].trim().to_owned()),
            });
        } else {
            eprintln!("fctool: {}: {}: invalid for statement", filename, line_nr + 1);
        }
    } else if command_line.starts_with("%foriter") {
        // let command_line = command_line.trim_end()["%foriter ".len()..].to_owned();
        state.for_data = Some(ForData {
            var_name: "line".to_owned(),
            input: None,
            lines: vec![],
            stack: 0,
            start_line_nr: line_nr,
            sep: None,
        });
    } else if command_line.starts_with("%func ") && command_line.trim_end() != "%func" {
        let command_line = command_line.trim_end()["%func ".len()..].to_owned();
        let split: Vec<&str> = command_line.split(' ').collect();
        state.func_data = Some(FuncData {
            name: split[0].to_owned(),
            arg_names: split.into_iter().skip(1).map(|a| a.to_owned()).collect(),
            lines: vec![],
            filename: filename.to_owned(),
            start_line_nr: line_nr,
        });
    } else {
        match VAR_REGEX.captures(command_line) {
            Some(captures) => {
                let mut command_line = captures[2].trim().to_owned();
                process_line(state, filename, line_nr, &mut command_line);
                if &captures[1] == "input" || &captures[1] == "exit" || &captures[1] == "source" {
                    eprintln!("fctool: {}: {}: {}: reserved variable name", filename, line_nr + 1, &captures[1]);
                }
                state.variables.last_mut().unwrap().insert(captures[1].to_owned(), command_line.clone());
                std::env::set_var(&captures[1], command_line);
            }
            None => {
                let mut command_line = command_line.to_owned();
                if command_line.trim_end() == "%l" {
                    command_line = "\n".to_owned();
                }
                process_line(state, filename, line_nr, &mut command_line);
                match &mut state.file.last_mut() {
                    Some(File::Regular(file, filename)) => { 
                        match file.write_all(command_line.as_bytes()) {
                            Ok(_) => {}
                            Err(err) => eprintln!("fctool: {}: {}: {}", filename, line_nr + 1, err)
                        }
                    }
                    Some(File::Stdout) => print!("{}", command_line),
                    Some(File::Stderr) => eprint!("{}", command_line),
                    Some(File::Null) => {}
                    Some(File::Process(proc)) => {
                        let _ = proc.stdin.as_mut().unwrap().write(command_line.as_bytes());
                    }
                    Some(File::Variable(var)) => {
                        let mut i = state.variables.len() - 1;
                        loop {
                            match state.variables[i].get_mut(var.as_str()) {
                                Some(a) => {
                                    a.push_str(&command_line);
                                    return;
                                }
                                None => {}
                            }
                            if i == 0 {
                                break;
                            }
                            i -= 1;
                        }
                        state.variables.last_mut().unwrap().insert(var.clone(), command_line);
                    }
                    None => {}
                }
            }
        }
    }
}

fn process_line(state: &mut State, filename: &str, line_nr: usize, line: &mut String) {
    let mut injections = vec![];
    while let Some(match_) = INJECT_REGEX.find(&line) {
        match PARSER.parse(filename, line_nr, &match_.as_str()[1..match_.as_str().len()-1]) {
            Ok(expr) => {
                injections.push(unwrap_expr(state, expr));
                line.replace_range(match_.range(), &format!("<<<injection {}>>>", injections.len() - 1));
            }
            Err(err) => {
                match err {
                    ParseError::UnrecognizedToken { expected, .. } => {
                        eprintln!("fctool: {}: {}: expected {}", filename, line_nr + 1, expected.join(", "));
                    }
                    ParseError::UnrecognizedEof { expected, .. } => {
                        eprintln!("fctool: {}: {}: expected {}", filename, line_nr + 1, expected.join(", "));
                    }
                    ParseError::InvalidToken { .. } => {
                        eprintln!("fctool: {}: {}: invalid token", filename, line_nr + 1);
                    }
                    _ => panic!()
                }
                injections.push("".to_owned());
                line.replace_range(match_.range(), &format!("<<<injection {}>>>", injections.len() - 1));
            }
        }
    }
    for (i, injection) in injections.into_iter().enumerate() {
        *line = line.replace(&format!("<<<injection {}>>>", i), &injection);
    }
}

fn unwrap_expr(state: &mut State, expr: Expr) -> String {
    match expr.data {
        ExprData::String(a) => match snailquote::unescape(&a) {
            Ok(b) => b,
            Err(err) => {
                eprintln!("fctool: {}: {}: {}", expr.loc.filename, expr.loc.line + 1, err);
                "".to_string()
            }
        },
        ExprData::Command(expr) => {
            let line = expr.loc.line;
            let filename = expr.loc.filename.clone();
            let command = unwrap_expr(state, *expr);
            let output = if cfg!(target_os = "windows") {
                Command::new("cmd")
                    .arg("/C")
                    .arg(command)
                    .output()
            } else {
                Command::new("sh")
                    .arg("-c")
                    .arg(command)
                    .output()
            };
            let output = match output {
                Ok(out) => {
                    match std::str::from_utf8(&out.stdout) {
                        Ok(a) => a.trim_end().to_owned(),
                        Err(err) => err.to_string(),
                    }
                }
                Err(err) => {
                    format!("fctool: {}: {}: command execution failed: {}", filename, line, err)
                }
            };
            output
        }
        ExprData::File(expr) => {
            let file = unwrap_expr(state, *expr).to_owned();
            let output = std::fs::read_to_string(&file);
            match output {
                Ok(a) => {
                    a
                }
                Err(err) => format!("fctool: {}: {}", file, err)
            }
        }
        ExprData::Concat(expra, exprb) => {
            let expra = unwrap_expr(state, *expra);
            let exprb = unwrap_expr(state, *exprb);
            expra + &exprb
        }
        ExprData::Variable(a) => {
            let mut i = state.variables.len() - 1;
            loop {
                match state.variables[i].get(&a) {
                    Some(a) => return a.to_owned(),
                    None => {},
                }
                if i == 0 {
                    break;
                }
                i -= 1;
            }
            String::new()
        }
        ExprData::Eq(expra, exprb) => {
            let expra = unwrap_expr(state, *expra);
            let exprb = unwrap_expr(state, *exprb);
            if expra == exprb {
                "true".to_owned()
            } else {
                "false".to_owned()
            }
        }
        ExprData::Neq(expra, exprb) => {
            let expra = unwrap_expr(state, *expra);
            let exprb = unwrap_expr(state, *exprb);
            if expra != exprb {
                "true".to_owned()
            } else {
                "false".to_owned()
            }
        }
        ExprData::And(expra, exprb) => {
            let expra = unwrap_expr(state, *expra);
            if expra == "false" {
                return "false".to_owned();
            }
            let exprb = unwrap_expr(state, *exprb);
            if exprb == "true" {
                return "true".to_owned();
            }
            "false".to_owned()
        }
        ExprData::Or(expra, exprb) => {
            let expra = unwrap_expr(state, *expra);
            if expra == "true" {
                return "true".to_owned();
            }
            let exprb = unwrap_expr(state, *exprb);
            if exprb == "true" {
                return "true".to_owned();
            }
            "false".to_owned()
        }
        ExprData::Input(prompt_expr) => {
            if let Some(prompt_expr) = prompt_expr {
                let prompt = unwrap_expr(state, *prompt_expr);
                print!("{}", prompt);
            }

            let _ = std::io::stdout().flush();
            let mut str = String::new();
            let _ = std::io::stdin().read_line(&mut str);
            return str.trim_end().to_owned();
        }
        ExprData::Exit => {
            std::process::exit(0);
        }
        ExprData::Source(expr) => {
            let file = unwrap_expr(state, *expr);
            let output = std::fs::read_to_string(&file);
            match output {
                Ok(a) => {
                    process_file(state, &file, &a);
                    "".to_owned()
                },
                Err(err) => err.to_string()
            }
        }
        ExprData::Add(expra, exprb) => {
            let expra = unwrap_expr(state, *expra);
            let exprb = unwrap_expr(state, *exprb);
            let Ok(a): Result<f64, _> = expra.trim().parse() else { return "".to_owned(); };
            let Ok(b): Result<f64, _> = exprb.trim().parse() else { return "".to_owned(); };
            format!("{}", a + b)
        }
        ExprData::Sub(expra, exprb) => {
            let expra = unwrap_expr(state, *expra);
            let exprb = unwrap_expr(state, *exprb);
            let Ok(a): Result<f64, _> = expra.trim().parse() else { return "".to_owned(); };
            let Ok(b): Result<f64, _> = exprb.trim().parse() else { return "".to_owned(); };
            format!("{}", a - b)
        }
        ExprData::Mul(expra, exprb) => {
            let expra = unwrap_expr(state, *expra);
            let exprb = unwrap_expr(state, *exprb);
            let Ok(a): Result<f64, _> = expra.trim().parse() else { return "".to_owned(); };
            let Ok(b): Result<f64, _> = exprb.trim().parse() else { return "".to_owned(); };
            format!("{}", a * b)
        }
        ExprData::Div(expra, exprb) => {
            let expra = unwrap_expr(state, *expra);
            let exprb = unwrap_expr(state, *exprb);
            let Ok(a): Result<f64, _> = expra.trim().parse() else { return "".to_owned(); };
            let Ok(b): Result<f64, _> = exprb.trim().parse() else { return "".to_owned(); };
            if b == 0.0 {
                return "".to_owned();
            }
            format!("{}", a / b)
        }
        ExprData::Mod(expra, exprb) => {
            let expra = unwrap_expr(state, *expra);
            let exprb = unwrap_expr(state, *exprb);
            let Ok(a): Result<f64, _> = expra.trim().parse() else { return "".to_owned(); };
            let Ok(b): Result<f64, _> = exprb.trim().parse() else { return "".to_owned(); };
            if b == 0.0 {
                return "".to_owned();
            }
            format!("{}", a % b)
        }
        ExprData::Pow(expra, exprb) => {
            let expra = unwrap_expr(state, *expra);
            let exprb = unwrap_expr(state, *exprb);
            let Ok(a): Result<f64, _> = expra.trim().parse() else { return "".to_owned(); };
            let Ok(b): Result<f64, _> = exprb.trim().parse() else { return "".to_owned(); };
            format!("{}", a.powf(b))
        }
        ExprData::LessThan(expra, exprb) => {
            let expra = unwrap_expr(state, *expra);
            let exprb = unwrap_expr(state, *exprb);
            let Ok(a): Result<f64, _> = expra.trim().parse() else { return "".to_owned(); };
            let Ok(b): Result<f64, _> = exprb.trim().parse() else { return "".to_owned(); };
            if a < b {
                "true".to_owned()
            } else {
                "false".to_owned()
            }
        }
        ExprData::MoreThan(expra, exprb) => {
            let expra = unwrap_expr(state, *expra);
            let exprb = unwrap_expr(state, *exprb);
            let Ok(a): Result<f64, _> = expra.trim().parse() else { return "".to_owned(); };
            let Ok(b): Result<f64, _> = exprb.trim().parse() else { return "".to_owned(); };
            if a > b {
                "true".to_owned()
            } else {
                "false".to_owned()
            }
        }
        ExprData::LessThanOrEq(expra, exprb) => {
            let expra = unwrap_expr(state, *expra);
            let exprb = unwrap_expr(state, *exprb);
            let Ok(a): Result<f64, _> = expra.trim().parse() else { return "".to_owned(); };
            let Ok(b): Result<f64, _> = exprb.trim().parse() else { return "".to_owned(); };
            if a <= b {
                "true".to_owned()
            } else {
                "false".to_owned()
            }
        }
        ExprData::MoreThanOrEq(expra, exprb) => {
            let expra = unwrap_expr(state, *expra);
            let exprb = unwrap_expr(state, *exprb);
            let Ok(a): Result<f64, _> = expra.trim().parse() else { return "".to_owned(); };
            let Ok(b): Result<f64, _> = exprb.trim().parse() else { return "".to_owned(); };
            if a >= b {
                "true".to_owned()
            } else {
                "false".to_owned()
            }
        }
        ExprData::Nth(expra, exprb, exprc) => {
            let input = unwrap_expr(state, *expra);
            let separator = match exprb {
                Some(sep) => unwrap_expr(state, *sep),
                None => format!("\n"),
            };
            let Ok(mut index): Result<i64, _> = unwrap_expr(state, *exprc).parse() else {
                return "".to_owned();
            };
            let split: Vec<&str> = input.split(&separator).collect();
            if index < 0 {
                index = input.len() as i64 - index;
            }
            split[index as usize].to_owned()
        }
        ExprData::Stdin =>  {
            let mut str = String::new();
            match std::io::stdin().read_to_string(&mut str) {
                Ok(_) => str,
                Err(err) => {
                    eprintln!("fctool: {}: {}: can't read stdin: {}", expr.loc.filename, expr.loc.line + 1, err);
                    String::new()
                }
            }
        }
        ExprData::Eval(expr) => {
            let filename = expr.loc.filename.clone();
            let line = expr.loc.line;
            let eval = unwrap_expr(state, *expr);
            match PARSER.parse("<eval>", 0, &eval) {
                Ok(expr) => {
                    unwrap_expr(state, expr)
                }
                Err(err) => {
                    match err {
                        ParseError::UnrecognizedToken { expected, .. } => {
                            eprintln!("fctool: {}: {}: expected {}", filename, line + 1, expected.join(", "));
                        }
                        ParseError::UnrecognizedEof { expected, .. } => {
                            eprintln!("fctool: {}: {}: expected {}", filename, line + 1, expected.join(", "));
                        }
                        ParseError::InvalidToken { .. } => {
                            eprintln!("fctool: {}: {}: invalid token", filename, line + 1);
                        }
                        _ => panic!()
                    }
                    String::new()
                }
            }
        }
        ExprData::Call(func_name, args) => {
            let mut unwrapped_args = vec![];
            for arg in args {
                unwrapped_args.push(unwrap_expr(state, arg));
            }
            let Some(func) = state.functions.get(&func_name) else {
                eprintln!("fctool: {}: {}: {}: function not defined", expr.loc.filename, expr.loc.line + 1, func_name);
                return String::new();
            };
            let filename = func.filename.clone();
            if unwrapped_args.len() != func.arg_names.len() {
                return String::new();
            }
            let mut vars = HashMap::new();
            for i in 0..unwrapped_args.len() {
                vars.insert(func.arg_names[i].clone(), unwrapped_args[i].clone());
            }
            state.variables.push(vars);
            let mut line_nr = func.start_line_nr;
            for code_line in &func.lines.clone() {
                parse_line(state, &filename, line_nr, code_line);
                if let LoopState::Return(ret_val) = std::mem::replace(&mut state.loop_state, LoopState::None) {
                    state.variables.pop();
                    return ret_val;
                }
                line_nr += 1;
            }
            state.variables.pop();
            String::new()
        }
        ExprData::Match(expra, exprb) => {
            let expra = unwrap_expr(state, *expra);
            let exprb = unwrap_expr(state, *exprb);
            match regex::Regex::new(&expra) {
                Ok(re) => {
                    if re.is_match(&exprb) {
                        "true".to_owned()
                    } else {
                        "false".to_owned()
                    }
                }
                Err(_) => "".to_owned()
            }
        }
        ExprData::In(expra, exprb) => {
            let expra = unwrap_expr(state, *expra);
            let exprb = unwrap_expr(state, *exprb);
            if exprb.contains(&expra) {
                "true".to_owned()
            } else {
                "false".to_owned()
            }
        }
        ExprData::Trim(expra, _exprb) => {
            let expr = unwrap_expr(state, *expra);
            expr.trim().to_owned()
        }
        ExprData::Filter(expra, exprb, exprc) => {
            let input = unwrap_expr(state, *expra);
            let separator = match exprb {
                Some(sep) => unwrap_expr(state, *sep),
                None => format!("\n"),
            };
            let split: Vec<&str> = input.split(&separator).collect();
            let mut out = String::new();
            let exprc = unwrap_expr(state, *exprc);
            match regex::Regex::new(&exprc) {
                Ok(re) => {
                    for line in split {
                        if re.is_match(line) {
                            out.push_str(line);
                            out.push_str(&separator);
                        }
                    }
                }
                Err(_) => {}
            }
            out
        }
        ExprData::Exclude(expra, exprb, exprc) => {
            let input = unwrap_expr(state, *expra);
            let separator = match exprb {
                Some(sep) => unwrap_expr(state, *sep),
                None => format!("\n"),
            };
            let split: Vec<&str> = input.split(&separator).collect();
            let mut out = String::new();
            let exprc = unwrap_expr(state, *exprc);
            match regex::Regex::new(&exprc) {
                Ok(re) => {
                    for line in split {
                        if !re.is_match(line) {
                            out.push_str(line);
                            out.push_str(&separator);
                        }
                    }
                }
                Err(_) => {}
            }
            out
        }
    }
}
