use structopt::StructOpt;


#[derive(StructOpt)]
#[structopt()]
pub struct Opt {

    #[structopt()]
    pub filename: Option<String>,

    #[structopt(short)]
    pub input: Option<String>, 

    #[structopt(short)]
    pub linemode: bool,

    #[structopt()]
    pub vars: Vec<String>,

}
